/**
 * Created by Shovon on 05/09/17.
 */
'use strict';
app.controller('storeLocatorCtrl',function ($scope,$stateParams,leafletData,$http,GetData,config) {
  $scope.storeLocatorControl = 'nav-close';
  $scope.toggleStoreLocatorControl = function(){
    $scope.storeLocatorControl = checkStoreLocatorControl($scope.storeLocatorControl);
  };
  $scope.buttonText = 'ALL';
  $scope.districtName = 'ALL';
  var checkStoreLocatorControl = function(status){
    if (status === 'nav-close' ){
      status = 'nav-open';
    }
    else
      status = 'nav-close';
    console.log(status);
    return status;
  };
  $scope.catStatus = 'all';
  var icons = {
    finance: {
      type: 'div',
      iconSize: [41, 37],
      className: 'branded-retail',
      iconAnchor:  [20.5, 18.5]
    },
    security: {
      type: 'div',
      iconSize: [41, 37],
      className: 'qubee-store',
      iconAnchor:  [20.5, 18.5]
    }
  };
  angular.extend($scope, {
    london: {
      lat: 23.815183,
      lng: 90.411205,
      zoom: 11
    },
    layers: {
      baselayers: {
        openStreetMap: {
          name: 'OpenStreetMap',
          type: 'xyz',
          url: 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
        }
      },
      overlays: {
        security: {
          type: 'group',
          name: 'security',
          visible: true
        },

        finance: {
          type: 'group',
          name: 'finance',
          visible: true
        }
      }
    },
    markers: {},
    toggleLayer: function(type,text)
    {
      $scope.buttonText = text;
      $scope.catStatus =type;

      var latlang = [];
      if (type === 'all'){
        latlang = [];
        //for (var i in $scope.layers.overlays){
        //  $scope.layers.overlays[i].visible = true;
        //}
        for(var cat in $scope.markers) {
          $scope.markers[cat].visible = true;
          latlang.push([$scope.markers[cat].lat,$scope.markers[cat].lng]);
        }
        console.log(latlang);
        $scope.centerJSON(latlang);
      }
      else {
        latlang = [];

        //for (var i in $scope.layers.overlays){
        //  $scope.layers.overlays[i].visible = false;
        //}
        //$scope.layers.overlays[type].visible = true;
        for(var cat in $scope.markers) {
          if ($scope.markers[cat].layer === type){
            latlang.push([$scope.markers[cat].lat,$scope.markers[cat].lng]);
            $scope.markers[cat].visible = true;
          }
          else{
            $scope.markers[cat].visible = false;
          }
        }
        console.log(latlang);
        $scope.centerJSON(latlang);
      }
    },
    updateMap: function (name) {
      $scope.storeLocatorControl = 'nav-close';
      $scope.markers[name].focus = true;
      $scope.london.lat = $scope.markers[name].lat;
      $scope.london.lng = $scope.markers[name].lng;
      $scope.london.zoom = 17;
      $scope.place = "";
      console.log("name",name);
    },
    districtFilter: (function(district) {
      var latlang = [];
      $scope.districtName = district;
      if (district === 'All'){
        latlang = [];
        //for (var i in $scope.layers.overlays){
        //  $scope.layers.overlays[i].visible = true;
        //}
        for(var cat in $scope.markers) {
          $scope.markers[cat].visible = true;
          latlang.push([$scope.markers[cat].lat,$scope.markers[cat].lng]);
        }
        console.log(latlang);
        $scope.centerJSON(latlang);
      }
      else{
        latlang = [];
        angular.forEach($scope.markers,(function(value,key){
          if(value.district.toLowerCase() == district.toLowerCase()){
            $scope.markers[key].visible = true;
            latlang.push([value.lat,value.lng]);
          }
          else{
            $scope.markers[key].visible = false;
          }
        }));
      }

      $scope.centerJSON(latlang);
      console.log("District",district);
    })
  });
  //$http({
  //  method: 'GET',
  //  url: 'res/geo.json'
  //}).then(function(d) {
  //  $scope.locator = d;
  //  angular.extend($scope, {
  //    markers: d.branches
  //  });
  //  angular.forEach($scope.markers, function(obj){
  //    console.log(obj);
  //    obj.message = '<h3>'+obj.name+'</h3><p><strong>Address: </strong>'+obj.address+'</p><p><strong>Telephone:</strong>'+obj.telephone+'</p><p><strong>Fax: </strong>'+obj.fax+'</p>';
  //  });
  //},function (error){
  //  console.log("error",error);
  //});
  $http({
    method: 'GET',
    url: config.urlMaker('contact-us')
    //url: 'res/geo.json'
  })
  .then(function (success){
    console.log('success',success);
    // return success.data;
      $scope.data = success.data;
    angular.extend($scope, {
      markers: success.data.branches
    });
    angular.forEach($scope.markers, function(obj){
      //`string text ${expression} string text`
      console.log(obj);
      //obj.message = `<h3>${obj.name}</h3>
      //<p><strong>Address: </strong> ${obj.address}</p>
      //<p><strong>Telephone:</strong> ${obj.telephone}</p>
      //<p><strong>Fax: </strong> ${obj.fax}</p>`;
      obj.visible = true;
      obj.message = '<h3>'+obj.name+'</h3><p><strong>Address: </strong>'+obj.address+'</p><p><strong>Telephone:</strong>'+obj.phone+'</p><p><strong>Fax: </strong>'+obj.fax+'</p>';
    });
  },function (error){
    console.log("error",error);
  });
  //$http.get("/").success(function(data, status) {
  //  angular.extend($scope, {
  //    markers: data
  //  });
  //});
  $scope.place = null;
  $scope.autocompleteOptions = {
    componentRestrictions: { country: 'bd' },
    types: ['geocode']
  };
  //$scope.updateMap = function () {
  //  if ($scope.place.geometry){
  //    $scope.london.lat = $scope.place.geometry.location.lat();
  //    $scope.london.lng = $scope.place.geometry.location.lng();
  //    $scope.london.zoom = 12;
  //  }
  //  else {
  //    $scope.london.zoom = 2;
  //  }
  //};
  //$scope.districtFilter = ((district) => {
  //  console.log("District",district);
  //});
  $scope.$watch('place', function(v){
    console.log("place",$scope.place);
    if (typeof $scope.place === 'object' && $scope.place){

      $scope.london.lat = $scope.place.geometry.location.lat();
      $scope.london.lng = $scope.place.geometry.location.lng();
      //$scope.updateMap();
    }
  });
  $scope.centerJSON = function(latlng) {
    console.log("center JSON");
    leafletData.getMap().then(function(map) {
      //var latlngs = [51.4638,-0.1677];
      map.fitBounds(latlng);
    });
  };
});
