app.controller('searchController', function ($scope,config,$stateParams,$http,$rootScope) {
  console.log("from search Ctrl---------",$stateParams);
  $scope.query = {query: $stateParams.query};
  $scope.results = {};
  $scope.statusMsg = "Loading......" ;
  $http({
    method: 'POST',
    headers : { 'Content-Type': 'application/x-www-form-urlencoded'},
    url: config.apiUrl+'search',
    data: $.param($scope.query)
  }).then(function successCallback(response) {
    $scope.results = response.data;
    console.log("Search success----",$scope.results.length);
    if ($scope.results.length > 0){
      $scope.statusMsg = true;
    }
    else {
      $scope.statusMsg = false;
    }
  }, function errorCallback(response) {
    $scope.results = response.data;
    $scope.statusMsg = false;
    console.log("Search Failed----",response);
    // called asynchronously if an error occurs
    // or server returns response with an error status.
  });
});
