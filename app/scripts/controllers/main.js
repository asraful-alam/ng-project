'use strict';

/**
 * @ngdoc function
 * @name idlcApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the idlcApp
 */
app.filter('trusted', ['$sce', function ($sce) {
  return function(url) {
    return $sce.trustAsResourceUrl(url);
  };
}]);
app.filter('removeHTMLTags', function() {
  console.log("Remove html filter");
  return function(text) {
    return  text ? String(text).replace(/\s/g, "").replace(/<[^>]+>/gm, '') : '';
  };
});
app.directive('iframeOnload', [function(){
  return {
    scope: {
      callBack: '&iframeOnload'
    },
    link: function(scope, element, attrs){
      element.on('load', function(){
        return scope.callBack();
      })
    }
  }}]);
//app.controller('ModalDemoCtrl', function ($uibModal, $log, $document,$scope) {
//  var $ctrl = this;
//  $ctrl.open = function (size, file) {
//    $scope.researchData.slug = file;
//    var modalInstance = $uibModal.open({
//      animation: true,
//      ariaLabelledBy: 'modal-title',
//      ariaDescribedBy: 'modal-body',
//      templateUrl: 'myModalContent.html',
//      controller: 'ModalInstanceCtrl',
//      controllerAs: '$ctrl',
//      size: size
//    });
//  };
//});
app.controller('ModalInstanceCtrl', function ($uibModalInstance,$scope,file,$http,config,GetData) {
  var $ctrl = this;
  $ctrl.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
  $scope.researchData = {};
  $scope.researchData.slug = file;
  console.log("File",file);
  if(file == 'bo'){
    GetData.async(config.urlMaker('terms-and-conditions')).then(function(d) {
      console.log("GetData",d);
      $scope.tnc = d;
    });
  }
  $scope.submit = function(){
    $http({
      method: 'POST',
      url: config.urlMaker('research/getPremiumReport'),
      headers : { 'Content-Type': 'application/x-www-form-urlencoded'},
      data: $.param($scope.researchData)
    }).then(function successCallback(response) {
      $uibModalInstance.dismiss('cancel');
      swal("Done", "You successfully authenticate", "success");
    }, function errorCallback(response) {
      console.log("Error");
      swal("Sorry", response.data.error, "error");
    });
  };
});
app.controller('MainCtrl', function ($scope,GetData,config,$location, $anchorScroll,call,$state,$uibModal,$rootScope,$filter) {
  console.time("start");
  //$scope.$on('http-started', function (event, args) {
  //  $scope.loadStatus = true;
  //  console.log('http-started');
  //});
  //$scope.$on('http-finished', function (event, args) {
  //  $timeout(function () {
  //    $scope.loadStatus = false;
  //  }, 200);
  //  console.log('http-finished');
  //});
  $rootScope.lastUpdate = $filter('date')(new Date() , "medium");
  $scope.iframeLoadedCallBack = function(){
    // do stuff
    console.log("Loading");
  };
  $rootScope.navToggle = true;
  $scope.navOpen = function(){
    $rootScope.navToggle =! $rootScope.navToggle;
  };
  $scope.currentYear = new Date();
  GetData.async(config.urlMaker('setting')).then(function(d) {
    $scope.setting = d;
  });
  $scope.open = function(template,size,file){
    console.log("Click",file);
    var modalInstance = $uibModal.open({
      animation: true,
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: template,
      controller: 'ModalInstanceCtrl',
      controllerAs: '$ctrl',
      size: size,
      resolve: {
        file: function () {
          return file;
        }
      }
    });
  };
  $scope.searchData = {};
  $scope.search = function () {
    if ($scope.searchData.query){
      $state.go("search",{query: $scope.searchData.query});
    }
    else {
      console.log("Empty Data");
      return false;
    }
  };
  console.timeEnd("start");
});
app.controller('whatWeOfferCtrl', function ($scope,$stateParams,$state,config,GetData) {
  GetData.async(config.urlMaker('what-we-offer')).then(function(d) {
    $scope.data = d;
  });
});
app.controller('whatWeOfferSingleCtrl', function ($scope,$stateParams,$state,config,GetData) {
  GetData.async(config.urlMaker('what-we-offer/'+$stateParams.id)).then(function(d) {
    $scope.singleData = d;
  });
});
app.controller('knowledgeBaseCtrl', function ($scope,$stateParams,$state,config,GetData) {
  GetData.async(config.urlMaker('knowledge')).then(function(d) {
    $scope.data = d;
    if($scope.data.knowledge.articles.length > 0){
      $scope.limit = $scope.data.knowledge.articles.length;
    }
  });
});
app.controller('knowledgeBaseSingleCtrl', function ($scope,$stateParams,$state,config,GetData) {
  GetData.async(config.urlMaker('knowledge/'+$stateParams.id)).then(function(d) {
    $scope.singleData = d;
  });
});
app.controller('boAccountCtrl',function($scope,call,GetData,$stateParams,$uibModal){
  $scope.modalOpen = function(template,size,file){
    console.log("Click",file);
    var modalInstance = $uibModal.open({
      animation: true,
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: template,
      controller: 'ModalInstanceCtrl',
      controllerAs: '$ctrl',
      size: size,
      resolve: {
        file: function () {
          return "bo";
        }
      }
    });
  };
  console.log("$stateParams",$stateParams);
  $scope.dateOptions = {
    formatYear: 'yy',
    startingDay: 1
  };
  $scope.open = function($event) {
    $scope.popup.opened = true;
    $event.preventDefault();
    $event.stopPropagation();
  };
  $scope.popup = {
    opened: false
  };
  $scope.datePopup = {
    applicantInfo: {
      dateOfBirth: {
        opened: false,
        open: function($event){
          $scope.datePopup.applicantInfo.dateOfBirth.opened = true;
          $event.preventDefault();
          $event.stopPropagation();
        }
      },
      authDateOfBirth: {
        opened: false,
        open: function($event){
          $scope.datePopup.applicantInfo.authDateOfBirth.opened = true;
          $event.preventDefault();
          $event.stopPropagation();
        }
      },
      marriageAnniversary: {
        opened: false,
        open: function($event){
          $scope.datePopup.applicantInfo.marriageAnniversary.opened = true;
          $event.preventDefault();
          $event.stopPropagation();
        }
      }
    },
    jointInfo:{
      jointAccountHolderDob: {
        opened: false,
        open: function($event){
          $scope.datePopup.jointInfo.jointAccountHolderDob.opened = true;
          $event.preventDefault();
          $event.stopPropagation();
        }
      },
      marriageAnniversary: {
        opened: false,
        open: function($event){
          $scope.datePopup.jointInfo.marriageAnniversary.opened = true;
          $event.preventDefault();
          $event.stopPropagation();
        }
      }
    },
    byeLaw: {
      accountOpendDate: {
        opened: false,
        open: function($event){
          $scope.datePopup.byeLaw.accountOpendDate.opened = true;
          $event.preventDefault();
          $event.stopPropagation();
        }
      },
      issueDate: {
        opened: false,
        open: function($event){
          $scope.datePopup.byeLaw.issueDate.opened = true;
          $event.preventDefault();
          $event.stopPropagation();
        }
      },
      expiryDate: {
        opened: false,
        open: function($event){
          $scope.datePopup.byeLaw.expiryDate.opened = true;
          $event.preventDefault();
          $event.stopPropagation();
        }
      },
      dateOfBirth: {
        opened: false,
        open: function($event){
          $scope.datePopup.byeLaw.dateOfBirth.opened = true;
          $event.preventDefault();
          $event.stopPropagation();
        }
      },
      refDateOfBirth: {
        opened: false,
        open: function($event){
          $scope.datePopup.byeLaw.refDateOfBirth.opened = true;
          $event.preventDefault();
          $event.stopPropagation();
        }
      }
    },
    nomineeInfo: {
      nomineeOnePassportIssueDate: {
        opened: false,
        open: function($event){
          $scope.datePopup.nomineeInfo.nomineeOnePassportIssueDate.opened = true;
          $event.preventDefault();
          $event.stopPropagation();
        }
      },
      nomineeOnePassportExpiryDate: {
        opened: false,
        open: function($event){
          $scope.datePopup.nomineeInfo.nomineeOnePassportExpiryDate.opened = true;
          $event.preventDefault();
          $event.stopPropagation();
        }
      },
      nomineeOneDob: {
        opened: false,
        open: function($event){
          $scope.datePopup.nomineeInfo.nomineeOneDob.opened = true;
          $event.preventDefault();
          $event.stopPropagation();
        }
      },
      nomineeOneMinorDob: {
        opened: false,
        open: function($event){
          $scope.datePopup.nomineeInfo.nomineeOneMinorDob.opened = true;
          $event.preventDefault();
          $event.stopPropagation();
        }
      },
      nomineeOneMaturityDate: {
        opened: false,
        open: function($event){
          $scope.datePopup.nomineeInfo.nomineeOneMaturityDate.opened = true;
          $event.preventDefault();
          $event.stopPropagation();
        }
      },
      nomineeOneMinorPassportIssueDate: {
        opened: false,
        open: function($event){
          $scope.datePopup.nomineeInfo.nomineeOneMinorPassportIssueDate.opened = true;
          $event.preventDefault();
          $event.stopPropagation();
        }
      },
      nomineeOneMinorPassportExpiryDate: {
        opened: false,
        open: function($event){
          $scope.datePopup.nomineeInfo.nomineeOneMinorPassportExpiryDate.opened = true;
          $event.preventDefault();
          $event.stopPropagation();
        }
      }
    },
    powerOfAttorney: {
      attorneyHolderDate: {
        opened: false,
        open: function($event){
          $scope.datePopup.powerOfAttorney.attorneyHolderDate.opened = true;
          $event.preventDefault();
          $event.stopPropagation();
        }
      },
      attorneyHolderExpireDate: {
        opened: false,
        open: function($event){
          $scope.datePopup.powerOfAttorney.attorneyHolderExpireDate.opened = true;
          $event.preventDefault();
          $event.stopPropagation();
        }
      },
      attorneyHolderDob: {
        opened: false,
        open: function($event){
          $scope.datePopup.powerOfAttorney.attorneyHolderDob.opened = true;
          $event.preventDefault();
          $event.stopPropagation();
        }
      },
      attorneyEffectiveFrom: {
        opened: false,
        open: function($event){
          $scope.datePopup.powerOfAttorney.attorneyEffectiveFrom.opened = true;
          $event.preventDefault();
          $event.stopPropagation();
        }
      },
      attorneyEffectiveTo: {
        opened: false,
        open: function($event){
          $scope.datePopup.powerOfAttorney.attorneyEffectiveTo.opened = true;
          $event.preventDefault();
          $event.stopPropagation();
        }
      }
    }
  };
  $scope.boInfo = {};
  if ($stateParams.shortBo){
    $scope.boInfo['applicant_info'] = {};
    Object.assign($scope.boInfo['applicant_info'],$stateParams.shortBo);
  }
  $scope.formHold = false;
  $scope.postBo = function() {
    $scope.formHold = true;
    console.log("postBo");
    call.postOrder($.param($scope.boInfo),'form/boForm', function(data) {
      $scope.formHold = false;
      console.log(data.data);
      if(data.data.error){
        swal("Sorry", "Something wrong", "error");
      }
      else{
        $scope.formData = {};
        swal("Good job!", "You clicked the button!", "success");
      }
    })
  }
});
app.controller('generalCtrl', function ($scope,$stateParams,$state,config,GetData,$filter,$window) {
  //$scope.activePage = $state.$current.parent.url.segments[1].replace('/','') || $state.current.url.replace('/','');
  $scope.stateParams = $stateParams;
  console.log("URL",$state.$current.parent.parent);
  $scope.activePage = $state.current.url.replace('/','');
  //$state.includes('whatWeOffer.single') ? $scope.activePage = 'what-we-offer' : $scope.activePage = $state.current.url.replace('/','')
  console.log('state',$state.includes('whatWeOffer.single'));
  GetData.async(config.urlMaker($scope.activePage)).then(function(d) {
    $scope.data = d;
  });
  $scope.expended = function(id){
    return $stateParams.id == id;
  };
  $scope.leadership = function(type,id){
    console.log("Type",type);
    console.log("ID",id);
    $state.go('whoWeAreAll',{cat: type,id: id}).then(function(){
      console.log('Done',$stateParams);
    });
  };
  $scope.type = '';
  $scope.tabToggle = function(type){
    $scope.type = type;
  };
  $scope.oneAtATime = true;
  $scope.status = {
    isCustomHeaderOpen: false,
    isFirstOpen: true,
    isFirstDisabled: false
  };
  var today = new Date();
  //today.setMonth(7);
  //today.setDate(25);
  if (today.getDay() != 5){
    $scope.date = $filter('date')(today, "yyyy-MM-dd");
  }
  else{
    $scope.date = $filter('date')(today.setDate(today.getDate() - 1),"yyyy-MM-dd");
  }
  $scope.alertMaker = function(){
    swal({
      title: "Server not responding",
      allowOutsideClick: false,
      type: 'warning'
    }).then(function () {
        $state.go('home');
        //$window.location.reload();
    })
  };
});
