'use strict';

/**
 * @ngdoc overview
 * @name idlcApp
 * @description
 * # idlcApp
 *
 * Main module of the application.
 */
/*
@home
@whoWeAre
  @team
@whatWeOffer
  @Single Offer
@research
@download
@sellSideResearch
@clickToTrade
@marketWatch
@whyInvestInBangladesh
@contactUs
*/
var app = angular
  .module('idlcApp', [
    'ngSanitize','ui.router','ui.bootstrap','ui-leaflet','google.places','oitozero.ngSweetAlert','angular-owl-carousel','ngAnimate','highcharts-ng'
  ]);
app.run(function($rootScope,$location,$state){
  //$rootScope.$on('$stateChangeSuccess',function(event, toState, toParams, fromState, fromParams){
  //  $rootScope.navStatus = false;
  //});
  $rootScope.$on('$stateChangeSuccess', function() {
    document.body.scrollTop = document.documentElement.scrollTop = 0;
    $rootScope.navToggle = false;
  });
});
