/**
 * Created by Shovon on 23/08/17.
 */
app.factory('GetData', function($http) {
  var data = {
    async: function(pram) {
      var promise = $http.get(pram).then(function (response) {
        console.log(response);
        return response.data;
      },function errorCallback(response) {
        return [];
      });
      return promise;
    }
  };
  return data;
});
