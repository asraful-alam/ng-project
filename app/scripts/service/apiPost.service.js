/**
 * Created by Shovon on 12/09/17.
 */
app.factory('call', function($http,config) {
//this is the key, as you can see I put the 'callBackFunc' as parameter
  function postOrder(dataArray,endpoint,callBackFunc) {
    $http({
      method: 'POST',
      headers : { 'Content-Type': 'application/x-www-form-urlencoded'},
      url: config.apiUrl+endpoint,
      data: dataArray
    }).then(function (data){
      callBackFunc(data);
    },function (error){
      console.log("error",error);
    });
  }
  return {
    postOrder:postOrder
  }
});
