/**
 * Created by Shovon on 23/08/17.
 */
/*
 @home
 @whoWeAre
 @team
 @whatWeOffer
 @singleOffer
 @research
 @download
 @sellSideResearch
 @marketWatch
 @whyInvestInBangladesh
 @contactUs
 */
app
  .config(function($stateProvider,$urlRouterProvider,$httpProvider,$locationProvider){
    var parentRouteMaker = function(name,url,template,controller){
      return{
        name: name,
        url: url,
        templateUrl: 'views/'+template,
        controller: controller || 'generalCtrl'
      }
    };
    var abstractRouteMaker = function(name,url,template){
      return{
        name: name,
        url: url,
        abstract: true,
        templateUrl: 'views/'+template
      }
    };
    $stateProvider
    .state(parentRouteMaker('home','/home','home.html'))
    .state(parentRouteMaker('whoWeAre','/who-we-are','whoWeAre.html'))
    .state(parentRouteMaker('whoWeAreAll','/who-we-are-leadership?cat&id','whoWeAreAll.html'))
    .state(parentRouteMaker('research','/research','research.html'))
    .state(parentRouteMaker('download','/download','download.html'))
    .state(parentRouteMaker('marketWatch','/market-watch','marketWatch.html'))
    .state(parentRouteMaker('whyInvestInBangladesh','/invest-in-bangladesh','whyInvestInBangladesh.html'))
    .state(parentRouteMaker('contactUs','/contact-us','contactUs.html','storeLocatorCtrl'))
    .state(abstractRouteMaker('whatWeOffer','/what-we-offer','whatWeOffer/index.html'))
    .state(parentRouteMaker('whatWeOffer.list','/list','whatWeOffer/list.html','whatWeOfferCtrl'))
    .state(parentRouteMaker('whatWeOffer.single','/:id','whatWeOffer/single.html','whatWeOfferSingleCtrl'))
    .state(abstractRouteMaker('knowledgeBase','/knowledge-base','knowledgeBase/index.html'))
    .state(parentRouteMaker('knowledgeBase.list','/list','knowledgeBase/list.html','knowledgeBaseCtrl'))
    .state(parentRouteMaker('knowledgeBase.single','/:id','knowledgeBase/single.html','knowledgeBaseSingleCtrl'))
    .state('boAccount',{
      url: "/bo-account",
      templateUrl: "views/boAccount.html",
      controller: 'boAccountCtrl',
      params: {
        shortBo: {},
        hiddenParam: 'YES'
      }
    })
    .state('search',{
      url: "/search?query",
      templateUrl: "views/search.html",
      controller: 'searchController'
    });
    $urlRouterProvider.otherwise('/home');
    //$locationProvider.html5Mode(true);
  //$stateProvider
  //.state('contactUs', {
  //    url: '/contact-us',
  //    templateUrl: 'views/contactUs.html',
  //    controller: 'storeLocatorCtrl'
  //});
  //.state('innerPage.cate', {
  //    url: '/:id',
  //    templateUrl: 'views/single.html'
  //})
  //.state('innerPage.cate.single', {
  //    url: '/:sid',
  //    template: '<h3>Inner Page Single</h3>'
  //})
  //.state('innerPage.cate', {
  //    url: '/:id',
  //    templateUrl: 'views/single.html',
  //    controller: 'SingleCtrl'
  //})
  //.state(parentRouteMaker('Shome','/s-home','Shome.html'))
  //.state(parentRouteMaker('SwhoWeAre','/s-who-we-are','SwhoWeAre.html'))
  //.state(parentRouteMaker('Steam','/s-team','Steam.html'))
  //.state(parentRouteMaker('SwhatWeOffer','/s-what-we-offer','SwhatWeOffer.html'))
  //.state(parentRouteMaker('SsingleOffer','/s-single-offer','SsingleOffer.html'))
  //.state(parentRouteMaker('Sresearch','/s-research','Sresearch.html'))
  //.state(parentRouteMaker('Sdownload','/s-download','Sdownload.html'))
  //.state(parentRouteMaker('SsellSideResearch','/s-sellside-research','SsellSideResearch.html'))
  //.state(parentRouteMaker('SmarketWatch','/s-market-watch','SmarketWatch.html'))
  //.state(parentRouteMaker('SwhyInvestInBangladesh','/s-why-invest-in-bangladesh','SwhyInvestInBangladesh.html'));
});
