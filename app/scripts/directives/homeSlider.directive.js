app
  .directive('homeSlider',function () {
    return{
      restrict: 'E',
      //controller: 'homeSliderCtrl',
      link: function(scope,element){
        scope.initOneCarousel = function() {
          setInterval(function(){
            $(element).owlCarousel({
              items: 1,
              autoplay: true,
              loop: true,
              nav: false
            });
            scope.$apply();
          }, 0);
        };
        //scope.initCarousel(element);
      }
    }
  }).directive('homeSliderItem',function() {
    return {
      restrict: 'E',
      transclude: false,
      link: function(scope, element) {
        // wait for the last item in the ng-repeat then call init
        if(scope.$last) {
          scope.initOneCarousel(element.parent());
        }
      }
    };
  })
.directive('accordionGroup', function() {
  return {
    require:'^accordion',         // We need this directive to be inside an accordion
    restrict:'EA',
    transclude:true,              // It transcludes the contents of the directive into the template
    replace: true,                // The element containing the directive will be replaced with the template
    templateUrl:'template/accordion/accordion-group.html',
    scope: {
      heading: '@',               // Interpolate the heading attribute onto this scope
      isOpen: '=?',
      isDisabled: '=?',
      initiallyOpen: '=?'
    },
    controller: function() {
      this.setHeading = function(element) {
        this.heading = element;
      };
    },
    link: function(scope, element, attrs, accordionCtrl) {
      accordionCtrl.addGroup(scope);

      if(scope.initiallyOpen) {
        scope.isOpen = true;
      }

      scope.$watch('isOpen', function(value) {
        if ( value ) {
          accordionCtrl.closeOthers(scope);
        }
      });

      scope.toggleOpen = function() {
        if ( !scope.isDisabled ) {
          scope.isOpen = !scope.isOpen;
        }
      };
    }
  };
})
