/**
 * Created by Shovon on 12/09/17.
 */
app
  .directive('multiCarouselSlider',function () {
    return{
        restrict: 'E',
        //controller: 'homeSliderCtrl',
        link: function(scope,element){
        scope.test= "dsd";
        scope.initCarousel = function(jElement) {
          console.log("initCarousel");
          setInterval(function(){
            $(jElement).owlCarousel({
              loop:false,
              dots:true,
              responsive:{
                0:{
                  items:1
                },
                600:{
                  items:4
                },
                1000:{
                  items:4,
                  margin:20
                }
              }
            });
            scope.$apply();
          }, 0);

          console.log("dsdsd");
        };
        //scope.initCarousel(element);
      }
    }
  }).directive('multiCarouselSliderItem',function() {
    return {
      restrict: 'AE',
      link: function(scope, element) {
        // wait for the last item in the ng-repeat then call init
        console.log("multiCarouselSliderItem",element.parent());
        if(scope.$last) {
         scope.initCarousel(element.parent());
          console.log("last",scope.test);
        }
      }
    };
  });
