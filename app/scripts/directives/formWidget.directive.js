/**
 * Created by Shovon on 13/09/17.
 */
'use strict';
app
  .directive('formWidget',function (call,$filter) {
    return{
      restrict: 'AE',
      scope: true,
      templateUrl: 'views/directive/formWidget.html',
      controller: function($scope){
        //$scope.type == 'call' ? $scope.formType = 'call' :  $scope.formType = 'account';
        $scope.formData = {};
        $scope.dateOptions = {
          formatYear: 'yy',
          maxDate: new Date(2020, 5, 22),
          minDate: new Date(),
          startingDay: 1
        };
        $scope.open = function($event) {
          $scope.popup.opened = true;
          $event.preventDefault();
          $event.stopPropagation();
        };
        $scope.popup = {
          opened: false
        };
        $scope.formHold = false;
        $scope.postOrder = function() {
          $scope.formHold = true;
          console.log("postorder");
          var formData = $scope.formData;
          formData.date = $filter('date')(formData.date, "dd-MM-yyyy");
          call.postOrder($.param(formData),'form/contactUs', function(data) {
            $scope.formHold = false;
            console.log(data.data);
            if(data.data.error){
              swal("Sorry", "Something wrong", "error");
            }
            else{
              $scope.formData = {};
              swal("Thank you!", "We will call you within 2 working days.", "success");
            }
          })
        }
      }
    }
  })
.directive('formWidgetBo',function ($state) {
    return{
      restrict: 'AE',
      scope: true,
      templateUrl: 'views/directive/formWidgetBo.html',
      controller: function($scope){
        //$scope.type == 'call' ? $scope.formType = 'call' :  $scope.formType = 'account';
        $scope.formData = {};
        $scope.postOrder = function() {
          $state.go('boAccount',{shortBo:  $scope.formData});
          //call.postOrder($.param(formData),'form/contactUs', function(data) {
          //  $scope.formHold = false;
          //  console.log(data.data);
          //  if(data.data.error){
          //    swal("Sorry", "Something wrong", "error");
          //  }
          //  else{
          //    $scope.formData = {};
          //    swal("Thank you!", "We will call you within 2 working days.", "success");
          //  }
          //})
        }
      }
    }
  });
