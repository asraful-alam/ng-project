app
  .directive('marketToday',function () {
    "use strict";
    return{
      restrict: 'AE',
      templateUrl: 'views/marketWatch/marketToday.directive.html',
      controller: function($scope,GetData,config,$filter,$rootScope){
        var chartConfig = {
          chart: {
            type: 'area',
            height: 350
          },
          credits: {
            enabled: false
          },
          legend: {
            enabled: false
          },
          yAxis: {
            title: {
              text: null
            }
          },
          colors: ['pink'],
          series: [{
            threshold: null,
            data: [],
            name: 'Index',
            id: 'Value'
          }],
          title: {
            text: ''
          },
          xAxis: {
            categories: [],
            type: "datetime"
          }
        };
        $scope.dsesChart = JSON.parse(JSON.stringify(chartConfig));
        $scope.ds30Chart = JSON.parse(JSON.stringify(chartConfig));
        $scope.dsexChart = JSON.parse(JSON.stringify(chartConfig));
        $scope.dsesChart.title.text = "DSES Index";
        $scope.ds30Chart.title.text = "DS30 Index";
        $scope.dsexChart.title.text = "DSEX Index";
        GetData.async(config.urlMaker('market-watch/market-index/'+$scope.date)).then(function(response) {
          if(!angular.equals({}, response.marketIndexes) && !angular.equals([], response.marketIndexes)){
            angular.forEach(response.marketIndexes,function(value,key){
              if(value['INDEXNAME'] == 'DSES'){
                $scope.dsesChart.series[0].data.push(Number(value['INDEXVALUE']));
                $scope.dsesChart.xAxis.categories.push($filter('date')(value['TIME'] , "HH:mm:ss"));
              }
              else if(value['INDEXNAME'] == 'DS30'){
                $scope.ds30Chart.series[0].data.push(Number(value['INDEXVALUE']));
                $scope.ds30Chart.xAxis.categories.push($filter('date')(value['TIME'] , "HH:mm:ss"));
                if(key == response.marketIndexes.length -1) {
                  $rootScope.lastUpdate = $filter('date')(value['TIME'] , "medium");
                }
              }
              else if(value['INDEXNAME'] == 'DSEX'){
                $scope.dsexChart.series[0].data.push(Number(value['INDEXVALUE']));
                $scope.dsexChart.xAxis.categories.push($filter('date')(value['TIME'] , "HH:mm:ss"));
              }
            });
          }
          else{
            //$scope.alertMaker();
          }
          //$scope.$broadcast('http-finished');
        });
      },
      link: function(scope,element){

      }
    }
  })
  .directive('strengthMeter',function () {
  return{
    restrict: 'AE',
    templateUrl: 'views/marketWatch/strengthMeter.directive.html',
    controller: function($scope,config,GetData) {
      $scope.pieConfig = {
        chart: {
          plotBackgroundColor: null,
          plotBorderWidth: 0,
          plotShadow: false,
          marginTop: -50,
          marginBottom: -75,
          marginLeft: 0,
          marginRight: 0
        },
        title: {
          text: 'DSE Gainer Loser Meter',
          align: 'center',
          verticalAlign: 'top',
          y: 40
        },
        legend: {
          verticalAlign: 'bottom',
          labelFormatter: function () {
            return this.name + (' '+this.percentage.toFixed(1) + '%');
          }
        },
        credits: {
          enabled: false
        },

        plotOptions: {
          pie: {
            dataLabels: {
              enabled: true,
              distance: -50,
              style: {
                fontWeight: 'bold'
              }
            },
            showInLegend: true,
            startAngle: -90,
            endAngle: 90,
            center: ['50%', '75%'],
            colors: [
              '#61C46E',
              '#FF5A5A',
              '#00B0FF'
              // '#24CBE5'
            ]
          }
        },
        series: [{
          type: 'pie',
          name: 'Percentage',
          innerSize: '50%',
          data: []
        }]
      };
      GetData.async(config.urlMaker('market-watch/strength-meter/'+$scope.date)).then(function(response) {
        if(!angular.equals({}, response) && !angular.equals([], response)){
          var data = [];
          data.push(['Gainer',response.gainerP]);
          data.push(['Loser',response.looserP]);
          data.push(['Unchanged',response.unchangedP]);
          $scope.pieConfig.series = [{
            data: data
          }];
        }
        else{
          //$scope.alertMaker();
        }
      });
    },
    link: function(scope,element){

    }
  }
})
.directive('industryUpdate',function () {
    return {
      restrict: 'AE',
      templateUrl: 'views/marketWatch/industryUpdate.directive.html',
      controller: function($scope,config,GetData) {
        $scope.barConfig = {
          chart: {
            type: 'bar'
          },
          colors: [
            '#00B0FF',
            '#6f6f6f'
          ],
          title: {
            text: "Today's Value"
          },
          xAxis: {
            categories: [],
            title: {
              text: null
            }
          },
          yAxis: {
            min: 0,
            title: {
              text: null
            },
            labels: {
              overflow: 'justify'
            }
          },
          tooltip: {
            valueSuffix: ' m'
          },
          plotOptions: {
            bar: {
              dataLabels: {
                enabled: true
              }
            }
          },
          credits: {
            enabled: false
          },
          series: []
        };
        GetData.async(config.urlMaker('market-watch/sector-status/'+$scope.date)).then(function(response) {
          if(!angular.equals({}, response) && !angular.equals([], response)){
            var tValue = [],yValue = [];
            angular.forEach(response, function(value, key) {
              $scope.barConfig.xAxis.categories.push(key);
              console.log("value['TTVal']",value['TTVal']);
              tValue.push(Number(value['TTVal'].toFixed(1)));
              yValue.push(Number(value['YTTVal'].toFixed(1)));
            });
            $scope.barConfig.series.push({name: 'Today',data : tValue});
            $scope.barConfig.series.push({name: 'Yesterday',data : yValue});
          }
          else{
            //$scope.alertMaker();
          }
        });
      }
    }
  })
.directive('industryUpdateGainerLooser',function () {
  return {
    restrict: 'AE',
    templateUrl: 'views/marketWatch/industryUpdateGainerLooser.directive.html',
    controller: function($scope,GetData,config) {
      $scope.gainerLooserConfig = {
        chart: {
          type: 'bar'

        },
        colors: [
          '#61C46E',
          '#FF5A5A',
          '#00B0FF'


        ],

        title: {
          text: 'Gainer Loser'
        },
        credits: {
          enabled: false
        },
        xAxis: {
          categories: []
          //categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
        },
        yAxis: {
          min: 0,
          title: {
            text: null
          }
        },
        legend: {
          reversed: true
        },
        plotOptions: {
          series: {
            stacking: 'normal'
          }
        },
        series: []
      };
      GetData.async(config.urlMaker('market-watch/sector-status/'+$scope.date)).then(function(response) {
        if(!angular.equals({}, response) && !angular.equals([], response)){
          var gainer = [],loser = [],unchanged=[];
          angular.forEach(response, function(value, key) {
            $scope.gainerLooserConfig.xAxis.categories.push(key);
            gainer.push(value['gainer']);
            loser.push(value['looser']);
            unchanged.push(value['unchanged']);
          });
          $scope.gainerLooserConfig.series = [];
          $scope.gainerLooserConfig.series.push({name: 'Gainer',data : gainer});
          $scope.gainerLooserConfig.series.push({name: 'Looser',data : loser});
          $scope.gainerLooserConfig.series.push({name: 'Unchanged',data : unchanged});
        }
        else{
          //$scope.alertMaker();
        }
      });
    }
  }
})
.directive('topChart',function () {
    return {
      restrict: 'AE',
      templateUrl: 'views/marketWatch/topChart.directive.html',
      controller: function($scope,config,GetData) {
        var sparklineConfig = {
          chart: {
            margin: [0, 0, 0, 0],
            height: 50,
            style: {
              overflow: 'visible'
            },
            colors: [
              '#FF5A5A'
            ]
          },
          title: {
            text: ''
          },
          credits: {
            enabled: false
          },
          legend: {
            enabled: false
          },
          xAxis: {
            labels: {
              enabled: false
            },
            tickLength: 0
          },
          yAxis: {
            title: {
              text: null
            },
            labels: {
              enabled: false
            },
            tickLength: 0
          },
          series: []
        };
        var sparklineGenerator = function(obj){
          for(var i in obj){
            var tempArr = [];
            var tempArrColumn = [];
            obj[i].chart = JSON.parse(JSON.stringify(sparklineConfig));
            obj[i].column = JSON.parse(JSON.stringify(sparklineConfig));
            //obj[i].chart = {};
            for(var j in obj[i].TH){

              tempArr.push({x : Number(j)+1,y : obj[i].TH[j].CP});
              tempArrColumn.push({x : Number(j)+1,y : obj[i].TH[j].TTVol});
            }
            obj[i].chart.series[0] = {
              fillColor: 'rgba(124, 181, 236, 0.3)',
              type:'area',
              name: 'Price',
              data: tempArr
            };
            obj[i].column.series[0] = {
              fillColor: 'rgba(124, 181, 236, 0.3)',
              type:'column',
              name: 'Volume',
              data: tempArrColumn
            };
            //obj[i].chart.data = tempArr;

          }

          return obj;
        };
        GetData.async(config.urlMaker('market-watch/top-status/'+$scope.date)).then(function(response) {

          if(!angular.equals({}, response) && !angular.equals([], response)){
            $scope.topVal = sparklineGenerator(response.topVal);
            $scope.topVol = sparklineGenerator(response.topVol);
            $scope.topLooser = sparklineGenerator(response.topLooser);
            $scope.topGainer = sparklineGenerator(response.topGainer);
            $scope.topTrade = sparklineGenerator(response.topTrade);
          }
          else{
            //$scope.alertMaker();
          }
        });
      }
    }
})
.directive('newsWidget',function(){
    return {
      restrict: 'AE',
      templateUrl: 'views/marketWatch/newsWidget.directive.html',
      controller: function($scope,config,GetData){
        GetData.async(config.urlMaker('market-watch/news/'+$scope.date)).then(function(response) {
          if(!angular.equals({}, response) && !angular.equals([], response)){
            $scope.news = response;
          }
          else{
            //$scope.alertMaker();
          }
        });
      }
    }
});
