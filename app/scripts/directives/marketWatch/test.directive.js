app.directive('topValueTest',function () {
  return {
    restrict: 'AE',
    templateUrl: 'views/marketWatch/test.directive.html',
    controller: function($scope,$http,config,GetData) {
      var sparklineConfig = {
        chart: {
          margin: [0, 0, 0, 0],
          style: {
            overflow: 'visible'
          }
        },
        title: {
          text: ''
        },
        credits: {
          enabled: false
        },
        legend: {
          enabled: false
        },
        xAxis: {
          labels: {
            enabled: false
          },
          tickLength: 0
        },
        yAxis: {
          title: {
            text: null
          },
          labels: {
            enabled: false
          },
          tickLength: 0
        },
        series: []
      };
      var sparklineGenerator = function(obj){
        for(var i in obj){
          var tempArr = [];
          var tempArrColumn = [];
          obj[i].chart = JSON.parse(JSON.stringify(sparklineConfig));
          obj[i].column = JSON.parse(JSON.stringify(sparklineConfig));
          //obj[i].chart = {};
          for(var j in obj[i].TH){
            //console.log("inner",obj[i].TH[j]);
            tempArr.push({x : Number(j)+1,y : obj[i].TH[j].CP});
            tempArrColumn.push({x : Number(j)+1,y : obj[i].TH[j].TTVol});
          }
          obj[i].chart.series[0] = {
            fillColor: 'rgba(124, 181, 236, 0.3)',
            type:'area',
            name: 'Price',
            data: tempArr
          };
          obj[i].column.series[0] = {
            fillColor: 'rgba(124, 181, 236, 0.3)',
            type:'column',
            name: 'Volume',
            data: tempArrColumn
          };
          //obj[i].chart.data = tempArr;
          console.log("obj",obj[i].chart);
        }
        console.log("Fullobj",obj);
        return obj;
      };
      GetData.async(config.urlMaker('market-watch/top-status/'+$scope.date)).then(function(response) {
        console.log(response);
        if(!angular.equals({}, response) && !angular.equals([], response)){
          $scope.topVal = sparklineGenerator(response.topVal);
          $scope.topVol = sparklineGenerator(response.topVol);
          $scope.topLooser = sparklineGenerator(response.topLooser);
          $scope.topGainer = sparklineGenerator(response.topGainer);
          $scope.topTrade = sparklineGenerator(response.topTrade);
        }
        else{
          $scope.alertMaker();
        }
      });
      //$http({
      //  method: 'GET',
      //  url: 'http://192.168.2.129/idlc-stock/public/api/market-watch/top-status'
      //}).then(function successCallback(response) {
      //  //$scope.tableData = response.data;
      //  $scope.topVal = sparklineGenerator(response.data.topVal);
      //  $scope.topVol = sparklineGenerator(response.data.topVol);
      //  $scope.topLooser = sparklineGenerator(response.data.topLooser);
      //  $scope.topGainer = sparklineGenerator(response.data.topGainer);
      //  $scope.topTrade = sparklineGenerator(response.data.topTrade);
      //}, function errorCallback(response) {
      //
      //});
    }
  }
});
